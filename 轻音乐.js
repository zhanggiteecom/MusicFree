const axios = require('axios');
const cheerio = require('cheerio');

async function getTopListDetail() {
  try {
    const response = await axios.get('https://api.biyigu.com/api/music/list?type=3');
    const jsonData = response.data.data;

    const searchResults = jsonData.map(({ id, name, cover, singer, url }) => ({
      id,
      title: name,
      artist: singer,
      artwork: cover,
      album: name,
      url
    }));

    return {
      id: 'q',
      description: '适合一个人静静听的轻音乐',
      title: '纯音乐',
      musicList: searchResults
    };
  } catch (error) {
    console.log('请求失败', error);
    return null;
  }
}

module.exports = {
  platform: '轻音乐',
  version: '0.1.0',
  cacheControl: 'no-store',
  async getTopLists() {
    return [{
      title: "音乐",
      data: [{
        id: "q",
        description: "适合一个人静静听的轻音乐",
        coverImg: "新歌榜的封面",
        title: "纯音乐",
      }]
    }]
  },
  getTopListDetail
};